﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtainyanController : MonoBehaviour
{
    [SerializeField]
    private float scaleMin = 1;
    [SerializeField]
    private float scaleMax = 30;

    private Animator animator;

    Vector2 sPos;
    Quaternion sRot;

    float tx;
    float diag;

    float sDist = 0.0f, nDist = 0.0f;
    Vector3 initScale; 
    float v = 1.0f;

    private void Awake()
    {
        Initialize();
    }

    private void OnEnable()
    {
        //transform.LookAt(Camera.main.transform.position);
        //transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }

    private void Start()
    {
        //diag = Mathf.Sqrt(Mathf.Pow(Screen.width, 2) + Mathf.Pow(Screen.height, 2));
        //initScale = transform.localScale;
    }

    private void Update()
    {
        //Pinch();
    }

    private void Initialize()
    {
        animator = GetComponent<Animator>();
    }

    public void PlayDance()
    {
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        if (!stateInfo.IsName("dance")) 
        {
            animator.SetTrigger("isDance");
        }
        else
        {
            animator.speed = animator.speed == 0 ? 0.8f : 0;
            
        }
    }


    void Rotation()
    {
        Touch t1 = Input.GetTouch(0);
        if (t1.phase == TouchPhase.Began)
        {
            sPos = t1.position;
            sRot = transform.localRotation;
        }
        else if (t1.phase == TouchPhase.Moved || t1.phase == TouchPhase.Stationary)
        {
            tx = (t1.position.x - sPos.x) / Screen.width;
            transform.rotation = sRot;
            transform.Rotate(new Vector3(0, -90 * tx, 0), Space.World);
        }
    }

    void Scaling()
    {
        Touch t1 = Input.GetTouch(0);
        Touch t2 = Input.GetTouch(1);

        if (t2.phase == TouchPhase.Began)
        {
            sDist = Vector2.Distance(t1.position, t2.position);
        }
        else if ( (t1.phase == TouchPhase.Moved || t1.phase == TouchPhase.Stationary) &&
                  (t2.phase == TouchPhase.Moved || t2.phase == TouchPhase.Stationary))
        {
            nDist = Vector2.Distance(t1.position, t2.position);
            v = v + (nDist - sDist) / diag;
            sDist = nDist;

            // 限界値をオーバーした際の処理
            if (v > scaleMax)
            {
                v = scaleMax;
            }
            else if (v < scaleMin)
            {
                v = scaleMin;
            }

            transform.localScale = initScale * v;
        }
    }

    void Pinch()
    {
        if (Input.touchCount == 1)
        {
            Rotation();
        }

        if (Input.touchCount >= 2)
        {
            Scaling();
        }
    }
}
