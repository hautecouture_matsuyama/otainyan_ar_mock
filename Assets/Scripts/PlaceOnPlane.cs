﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlaceOnPlane : MonoBehaviour
{
    [SerializeField]
    private Text dText;

    [SerializeField]
    private GameObject otainyan;
    [SerializeField]
    private GameObject arCamera;

    private GameObject spawnedObject;
    private ARRaycastManager raycastManager;
    private static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private void Awake()
    {
        raycastManager = GetComponent<ARRaycastManager>();
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        {
            return;
        }

        if (Input.touchCount > 0)
        {
            Vector2 touchPos = Input.GetTouch(0).position;
            if(raycastManager.Raycast(touchPos, hits, TrackableType.Planes))
            {
                var hitPose = hits[0].pose;

                if (!spawnedObject)
                {
                    spawnedObject = Instantiate(otainyan, hitPose.position, Quaternion.identity);

                    foreach (var plane in GetComponent<ARPlaneManager>().trackables)
                    {
                        plane.gameObject.SetActive(false);
                    }

                    foreach (var point in GetComponent<ARPointCloudManager>().trackables)
                    {
                        point.gameObject.SetActive(false);
                    }
                }
                
            }
        }
    }

    public void Relocate()
    {
        Debug.Log("非表示");
        spawnedObject.SetActive(false);
        Destroy(spawnedObject);

        foreach (var plane in GetComponent<ARPlaneManager>().trackables)
        {
            plane.gameObject.SetActive(true);
        }

        foreach (var point in GetComponent<ARPointCloudManager>().trackables)
        {
            point.gameObject.SetActive(true);
        }
    }
}
