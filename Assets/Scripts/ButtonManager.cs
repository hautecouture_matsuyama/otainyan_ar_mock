﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    [SerializeField]
    private Button photoButton;
    [SerializeField]
    private Button pauseButton;
    [SerializeField]
    private Button wordsButton;

    [SerializeField]
    private Button shareButton;

    [SerializeField]
    private GameObject menuWindow;

    [SerializeField]
    private GameObject word;

    [SerializeField]
    private ScreenShot screenShot;


    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        photoButton.onClick.AddListener(() => OnClickPhoto());
        pauseButton.onClick.AddListener(() => OnClickPause());
        wordsButton.onClick.AddListener(() => OnClickWords());
    }

    public void OnClickPhoto()
    {
        screenShot.Execute();
    }

    public void OnClickPause()
    {
        shareButton.interactable = menuWindow.activeSelf;
        photoButton.interactable = menuWindow.activeSelf;
        wordsButton.interactable = menuWindow.activeSelf;

        menuWindow.SetActive(!menuWindow.activeSelf);
    }

    public void OnClickWords()
    {
        word.SetActive(!word.activeSelf);
    }
}
